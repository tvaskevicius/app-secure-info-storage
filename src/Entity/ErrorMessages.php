<?php
declare(strict_types=1);

namespace App\Entity;

final class ErrorMessages
{
    const ERROR_UNAUTHORIZED = 'Unauthorized';
    const ERROR_MISSING_PARAMETER = 'No data parameter';

    const ITEM_NOT_FOUND = 'No item';
}