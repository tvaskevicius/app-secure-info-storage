<?php
declare(strict_types=1);

namespace App\Security;

use App\Exception\DecryptException;
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;
use Defuse\Crypto\Key;

class DefuseEncryptor implements EncryptionInterface
{
    private string $defusorKey;

    /**
     * Encryptor constructor.
     * @param string $defusorKey
     */
    public function __construct(string $defusorKey)
    {
        $this->defusorKey = $defusorKey;
    }

    public function encryptString(string $data)
    {
        $key = $this->loadEncryptionKeyFromConfig();
        return Crypto::encrypt($data, $key);
    }

    public function decryptString(string $cipheredData)
    {
        $key = $this->loadEncryptionKeyFromConfig();
        try {
            return Crypto::decrypt($cipheredData, $key);
        } catch (WrongKeyOrModifiedCiphertextException $ex) {
            throw new DecryptException('Possible attack detected!');
        }
    }

    private function loadEncryptionKeyFromConfig()
    {
        return Key::loadFromAsciiSafeString($this->defusorKey);
    }
}
