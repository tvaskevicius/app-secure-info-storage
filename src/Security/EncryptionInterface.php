<?php

namespace App\Security;

interface EncryptionInterface
{
    public function encryptString(string $data);

    public function decryptString(string $data);
}