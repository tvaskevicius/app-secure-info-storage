<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Security\DefuseEncryptor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ItemFixture extends Fixture implements OrderedFixtureInterface
{
    private DefuseEncryptor $encryptor;
    
    public function __construct(DefuseEncryptor $encryptor)
    {
        $this->encryptor = $encryptor;
    }

    public function load(ObjectManager $manager)
    {
        $item = new Item();
        $item->setData($this->encryptor->encryptString('test data'));
        $item->setUser($this->getReference('user'));
        $manager->persist($item);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
