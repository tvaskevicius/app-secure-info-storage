<?php
declare(strict_types=1);

namespace App\Service\Normalizer;

use App\Entity\Item;
use App\Security\DefuseEncryptor;

class ItemNormalizer implements NormalizerInterface
{
    private DefuseEncryptor $encryptor;

    public function __construct(DefuseEncryptor $encryptor)
    {
        $this->encryptor = $encryptor;
    }

    /**
     * @param Item $object
     * @return array
     */
    public function mapFromEntity($object)
    {
        $data = [];
        if ($object->getCreatedAt() !== null) {
            $data['created_at'] = $object->getCreatedAt();
        }
        if ($object->getData() !== null) {
            $data['data'] = $this->encryptor->decryptString($object->getData());
        }
        if ($object->getId() !== null) {
            $data['id'] = $object->getId();
        }
        if ($object->getUpdatedAt() !== null) {
            $data['updated_at'] = $object->getUpdatedAt();
        }

        return $data;
    }
}