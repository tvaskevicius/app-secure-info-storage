<?php

namespace App\Service\Normalizer;

class ItemArrayNormalizer extends ArrayNormalizer
{
    public function __construct(ItemNormalizer $itemNormalizer)
    {
        parent::__construct($itemNormalizer);
    }
}