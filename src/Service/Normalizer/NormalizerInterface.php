<?php

namespace App\Service\Normalizer;

interface NormalizerInterface
{
    public function mapFromEntity(object $entity);
}