<?php
declare(strict_types=1);

namespace App\Service\Normalizer;

class ArrayNormalizer implements NormalizerInterface
{
    private NormalizerInterface $objectNormalizer;

    /**
     * ArrayNormalizer constructor.
     * @param NormalizerInterface $objectNormalizer
     */
    public function __construct(NormalizerInterface $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    /**
     * @param array $object
     * @return array
     */
    public function mapFromEntity($object)
    {
        $result = [];
        foreach ($object as $arrayItem) {
            $result[] = $this->objectNormalizer->mapFromEntity($arrayItem);
        }
        return $result;
    }
}
