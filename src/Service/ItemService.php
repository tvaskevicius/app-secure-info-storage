<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Item;
use App\Repository\ItemRepository;
use App\Security\DefuseEncryptor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ItemService
{
    private EntityManagerInterface $entityManager;
    private ItemRepository $itemRepository;
    private DefuseEncryptor $encryptor;

    public function __construct(
        EntityManagerInterface $entityManager,
        ItemRepository $itemRepository,
        DefuseEncryptor $encryptor
    ) {
        $this->entityManager = $entityManager;
        $this->itemRepository = $itemRepository;
        $this->encryptor = $encryptor;
    }

    public function createItem(UserInterface $user, string $data): void
    {
        $item = (new Item())
            ->setUser($user)
            ->setData($this->encryptor->encryptString($data))
        ;

        $this->entityManager->persist($item);
    }

    public function findItemsByUser(UserInterface $user)
    {
        return $this->itemRepository->findByUser($user);
    }

    public function deleteItem(Item $item)
    {
        $this->entityManager->remove($item);
    }

    /**
     * @param string $id
     * @return Item|null
     */
    public function findItemById(string $id)
    {
        return $this->itemRepository->find($id);
    }

    public function updateItem(Item $item, string $data)
    {
        return $item->setData($this->encryptor->encryptString($data));
    }
} 