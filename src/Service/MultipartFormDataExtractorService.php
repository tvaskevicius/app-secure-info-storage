<?php
declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class MultipartFormDataExtractorService
{
    /**
     * @param Request $request
     * @param string $key
     * @return string|null
     */
    public function getMultipartContentSplitByBoundary(Request $request, string $key): ?string
    {
        $boundary = explode("\r\n", $request->getContent())[0];
        $formDataSplitByBoundary = explode($boundary, $request->getContent());
        return $this->extractFormDataParameterByKey($formDataSplitByBoundary, $key);
    }

    private function extractFormDataParameterByKey(array $formDataSplitByBoundary, string $key): ?string
    {
        $result = null;
        foreach ($formDataSplitByBoundary as $formDataPart) {
            if (strpos($formDataPart, $key) !== false) {
                $items = explode("\r\n\r\n", $formDataPart);
                $result = trim($items[count($items) - 1], "\r\n");
            }
        }
        return $result;
    }
}