<?php

namespace App\Controller;

use App\Entity\ErrorMessages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login", methods={"POST"})
     */
    public function login(): JsonResponse
    {
        $user = $this->getUser();
        if ($user === null) {
            return $this->formatErrorJson(ErrorMessages::ERROR_UNAUTHORIZED, Response::HTTP_UNAUTHORIZED);
        }
        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ]);
    }

    /**
     * @Route("/logout", name="logout", methods={"POST"})
     */
    public function logout()
    {

    }

    /**
     * @Route("/logged-out", name="logged_out_route", methods={"GET"})
     */
    public function loggedOut()
    {
        return $this->json([]);
    }

    private function formatErrorJson(string $message, int $responseStatus = Response::HTTP_OK): JsonResponse
    {
        return $this->json(['error' => $message], $responseStatus);
    }
}
