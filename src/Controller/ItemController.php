<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\ErrorMessages;
use App\Service\ItemService;
use App\Service\MultipartFormDataExtractorService;
use App\Service\Normalizer\ItemArrayNormalizer;
use App\Service\Normalizer\ItemNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends AbstractController
{
    private ItemService $itemService;
    private ItemArrayNormalizer $itemArrayNormalizer;
    private ItemNormalizer $itemNormalizer;
    private MultipartFormDataExtractorService $multipartFormDataExtractorService;
    private EntityManagerInterface $entityManager;

    public function __construct(
        ItemService $itemService,
        ItemArrayNormalizer $itemArrayNormalizer,
        ItemNormalizer $itemNormalizer,
        MultipartFormDataExtractorService $multipartFormDataExtractorService,
        EntityManagerInterface $entityManager
    ) {
        $this->itemService = $itemService;
        $this->itemArrayNormalizer = $itemArrayNormalizer;
        $this->multipartFormDataExtractorService = $multipartFormDataExtractorService;
        $this->itemNormalizer = $itemNormalizer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/item", name="item_list", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function list(): JsonResponse
    {
        $user = $this->getUser();
        if ($user === null) {
            return $this->formatErrorJson(ErrorMessages::ERROR_UNAUTHORIZED, Response::HTTP_UNAUTHORIZED);
        }

        return $this->json($this->itemArrayNormalizer->mapFromEntity($this->itemService->findItemsByUser($user)));
    }

    /**
     * @Route("/item", name="item_create", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function create(Request $request, ItemService $itemService)
    {
        $user = $this->getUser();
        if ($user === null) {
            return $this->formatErrorJson(ErrorMessages::ERROR_UNAUTHORIZED, Response::HTTP_UNAUTHORIZED);
        }
        $data = $request->get('data');

        if (empty($data)) {
            return $this->formatErrorJson(ErrorMessages::ERROR_MISSING_PARAMETER, Response::HTTP_BAD_REQUEST);
        }

        $itemService->createItem($this->getUser(), $data);
        $this->entityManager->flush();
        return $this->json([]);
    }

    /**
     * @Route("/item/{id}", name="items_delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     */
    public function delete(Request $request, int $id)
    {
        $user = $this->getUser();
        if ($user === null) {
            return $this->formatErrorJson(ErrorMessages::ERROR_UNAUTHORIZED, Response::HTTP_UNAUTHORIZED);
        }

        if (empty($id)) {
            return $this->formatErrorJson(ErrorMessages::ERROR_MISSING_PARAMETER, Response::HTTP_BAD_REQUEST);
        }

        $item = $this->itemService->findItemById((string)$id);
        if ($item === null) {
            return $this->formatErrorJson(ErrorMessages::ITEM_NOT_FOUND, Response::HTTP_NOT_FOUND);
        }

        $this->itemService->deleteItem($item);
        $this->entityManager->flush();
        return $this->json([]);
    }

    /**
     * @Route("/item", name="items_update", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     */
    public function update(Request $request)
    {
        $user = $this->getUser();
        if ($user === null) {
            return $this->formatErrorJson(ErrorMessages::ERROR_UNAUTHORIZED, Response::HTTP_UNAUTHORIZED);
        }

        $id = $this->multipartFormDataExtractorService->getMultipartContentSplitByBoundary($request, 'id');
        if (empty($id)) {
            return $this->formatErrorJson(ErrorMessages::ERROR_MISSING_PARAMETER, Response::HTTP_BAD_REQUEST);
        }

        $data = $this->multipartFormDataExtractorService->getMultipartContentSplitByBoundary($request, 'data');
        if (empty($data)) {
            return $this->formatErrorJson(ErrorMessages::ERROR_MISSING_PARAMETER, Response::HTTP_BAD_REQUEST);
        }

        $itemToUpdate = $this->itemService->findItemById((string)$id);
        if ($itemToUpdate === null) {
            return $this->formatErrorJson(ErrorMessages::ITEM_NOT_FOUND, Response::HTTP_NOT_FOUND);
        }

        if ($itemToUpdate->getUser() !== $user) {
            return $this->formatErrorJson(ErrorMessages::ERROR_UNAUTHORIZED, Response::HTTP_UNAUTHORIZED);
        }

        if ($itemToUpdate->getData() === $data) {
            return $this->json($this->itemNormalizer->mapFromEntity($itemToUpdate));
        }

        $updatedItem = $this->itemService->updateItem($itemToUpdate, $data);
        $this->entityManager->flush();
        return $this->json($this->itemNormalizer->mapFromEntity($updatedItem));
    }

    private function formatErrorJson(string $message, int $responseStatus = Response::HTTP_OK): JsonResponse
    {
        return $this->json(['error' => $message], $responseStatus);
    }
}
