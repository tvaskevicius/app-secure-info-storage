<?php
declare(strict_types=1);

namespace App\Tests\Unit\Service\Security;

use App\Exception\DecryptException;
use App\Security\DefuseEncryptor;
use PHPUnit\Framework\TestCase;

class DefuseEncryptorTest extends TestCase
{
    private DefuseEncryptor $defuseEncryptor;

    protected function setUp(): void
    {
        $this->defuseEncryptor = new DefuseEncryptor(
            file_get_contents(__DIR__ . '/../../Resources/test_defusor_key.txt')
        );
    }

    public function testEncryptDecrypt()
    {
        $string = 'test_string';
        $encryptedString = $this->defuseEncryptor->encryptString($string);
        $decryptedString = $this->defuseEncryptor->decryptString($encryptedString);
        $this->assertEquals($string, $decryptedString);
    }

    public function testEncryptDecryptWithBadKey()
    {
        $this->expectException(DecryptException::class);
        $string = 'test_string';
        $encryptedString = $this->defuseEncryptor->encryptString($string);
        $this->defuseEncryptor = new DefuseEncryptor(
            file_get_contents(__DIR__ . '/../../Resources/bad_defusor_key.txt')
        );
        $this->defuseEncryptor->decryptString($encryptedString);
    }
}