<?php

namespace App\Tests\Unit\Service;

use App\Entity\Item;
use App\Entity\User;
use App\Repository\ItemRepository;
use App\Security\DefuseEncryptor;
use App\Service\ItemService;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;

class ItemServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private $entityManager;

    /**
     * @var ItemRepository|MockObject
     */
    private $itemRepository;

    /**
     * @var DefuseEncryptor|MockObject
     */
    private $encryptor;

    /**
     * @var ItemService
     */
    private $itemService;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->itemRepository = $this->createMock(ItemRepository::class);
        $this->encryptor = $this->createMock(DefuseEncryptor::class);

        $this->itemService = new ItemService($this->entityManager, $this->itemRepository, $this->encryptor);
    }

    public function testCreate(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);
        $data = 'secret data';

        $expectedObject = new Item();
        $expectedObject->setUser($user)->setData('secret data');

        $this->entityManager->expects($this->once())->method('persist')->with($expectedObject);
        $this->encryptor
            ->expects($this->once())
            ->method('encryptString')
            ->with($expectedObject->getData())
            ->willReturn($expectedObject->getData());

        $this->itemService->createItem($user, $data);
    }

    public function testUpdate(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);
        $item = new Item();
        $item->setUser($user)->setData('secret data');
        $this->encryptor
            ->expects($this->once())
            ->method('encryptString')
            ->with('encrypted updated data')
            ->willReturn('encrypted updated data')
        ;

        $this->assertEquals(
            'encrypted updated data',
            $this->itemService->updateItem($item, 'encrypted updated data')->getData()
        );
    }

    public function testFindItemsByUser(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);

        $expectedObject = new Item();
        $expectedObject->setUser($user)->setData('secret data');

        $this->itemRepository->expects($this->once())->method('findByUser')->with($user);

        $this->itemService->findItemsByUser($user);
    }

    public function findItemById(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);

        $expectedObject = new Item();
        $expectedObject->setUser($user)->setData('secret data');

        $this->itemRepository->expects($this->once())->method('find')->with($expectedObject->getId());

        $this->itemService->findItemById($expectedObject);
    }

    public function testDelete(): void
    {
        /** @var User */
        $user = $this->createMock(User::class);

        $expectedObject = new Item();
        $expectedObject->setUser($user)->setData('secret data');

        $this->entityManager->expects($this->once())->method('remove')->with($expectedObject);

        $this->itemService->deleteItem($expectedObject);
    }
}
