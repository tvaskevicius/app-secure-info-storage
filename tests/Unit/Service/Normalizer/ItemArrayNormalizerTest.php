<?php
declare(strict_types=1);

namespace App\Tests\Unit\Service\Normalizer;

use App\Entity\Item;
use App\Entity\User;
use App\Security\DefuseEncryptor;
use App\Service\Normalizer\ItemArrayNormalizer;
use App\Service\Normalizer\ItemNormalizer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use DateTimeImmutable;

class ItemArrayNormalizerTest extends TestCase
{
    private ItemArrayNormalizer $itemArrayNormalizer;

    /**
     * @var DefuseEncryptor|MockObject
     */
    private $encryptor;

    protected function setUp(): void
    {
        $this->encryptor = $this->createMock(DefuseEncryptor::class);
        $this->itemArrayNormalizer = new ItemArrayNormalizer(new ItemNormalizer($this->encryptor));
    }

    public function testMapFromEntity()
    {
        $item = (new Item())
            ->setData('data')
            ->setCreatedAt(new DateTimeImmutable('2020-01-01'))
        ;
        $secondItem = (new Item())
            ->setData('another one')
            ->setUser(new User())
            ->setUpdatedAt(new DateTimeImmutable('2020-10-01'))
        ;
        $this->encryptor
            ->expects($this->exactly(2))
            ->method('decryptString')
            ->willReturnOnConsecutiveCalls('data', 'another one')
        ;
        $this->assertJsonStringEqualsJsonString(
            '[{"created_at":{"date":"2020-01-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"data":"data"},{"data":"another one","updated_at":{"date":"2020-10-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"}}]',
            json_encode($this->itemArrayNormalizer->mapFromEntity([$item, $secondItem]))
        );
    }
}
