<?php
declare(strict_types=1);

namespace App\Tests\Unit\Service\Normalizer;

use App\Entity\Item;
use App\Security\DefuseEncryptor;
use App\Service\Normalizer\ItemNormalizer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use DateTimeImmutable;

class ItemNormalizerTest extends TestCase
{
    private ItemNormalizer $itemNormalizer;

    /**
     * @var DefuseEncryptor|MockObject
     */
    private $encryptor;

    protected function setUp(): void
    {
        $this->encryptor = $this->createMock(DefuseEncryptor::class);
        $this->itemNormalizer = new ItemNormalizer($this->encryptor);
    }

    public function testMapFromEntity()
    {
        $item = (new Item())
            ->setData('data')
            ->setCreatedAt(new DateTimeImmutable('2020-01-01'))
        ;
        $this->encryptor
            ->expects($this->once())
            ->method('decryptString')
            ->willReturn('data')
        ;
        $this->assertJsonStringEqualsJsonString(
            '{"created_at":{"date":"2020-01-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"data":"data"}',
            json_encode($this->itemNormalizer->mapFromEntity($item))
        );
    }
}
