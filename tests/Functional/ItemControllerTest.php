<?php

namespace App\Tests\Functional;

use App\Entity\ErrorMessages;
use App\Entity\Item;
use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class ItemControllerTest extends WebTestCase
{
    private $client;
    private $userRepository;
    private $itemRepository;

    protected function setUp()
    {
        $this->client = static::createClient();
        $this->userRepository = static::$container->get(UserRepository::class);
        $this->itemRepository = static::$container->get(ItemRepository::class);
    }

    /**
     * @dataProvider createDataProvider
     */
    public function testCreate(string $data, string $expectedString)
    {
        $this->loginUser();

        $newItemData = ['data' => $data];

        $this->client->request('POST', '/item', $newItemData);
        $this->client->request('GET', '/item');

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString($expectedString, $this->client->getResponse()->getContent());

        $this->userRepository->findOneByData($data);
    }

    public function createDataProvider()
    {
        return [
            'basic string' => ['very secure new item data', 'very secure new item data'],
            'php code string' => ['<?php phpinfo();', '\u003C?php phpinfo();'],
        ];
    }

    /**
     * @dataProvider updateDataProvider
     */
    public function testUpdate(string $stringToUpdate, string $expectedString, bool $isSuccess, int $errorCode = null)
    {
        $this->loginUser();
        /** @var Item $itemToUpdate */
        $itemToUpdate = $this->itemRepository->findAll()[0];
        $filledItemData = sprintf(
            '----------------------------230524922969835098721821&#13;&#10;Content-Disposition: form-data; name=&quot;id&quot;&#13;&#10;&#13;&#10;%d&#13;&#10;----------------------------230524922969835098721821&#13;&#10;Content-Disposition: form-data; name=&quot;data&quot;&#13;&#10;&#13;&#10;%s&#13;&#10;----------------------------230524922969835098721821--&#13;&#10;',
            $itemToUpdate->getId(),
            $stringToUpdate
        );
        $this->client->request('PUT', '/item', [], [], [], html_entity_decode($filledItemData));

        if ($isSuccess) {
            $this->assertResponseIsSuccessful();
        } else {
            $this->assertResponseStatusCodeSame($errorCode, $this->client->getResponse()->getStatusCode());
        }
        $this->assertStringContainsString($expectedString, $this->client->getResponse()->getContent());
    }

    public function updateDataProvider()
    {
        return [
            'basic string' => [
                'new updated secret',
                'new updated secret',
                true,
            ],
            'php code string' => [
                '<?php phpinfo();',
                '\u003C?php phpinfo();',
                true,
            ],
            'not found' => [
                '----------------------------230524922969835098721821&#13;&#10;Content-Disposition: form-data; name=&quot;id&quot;&#13;&#10;&#13;&#10;<?php phpinfo();&#13;&#10;----------------------------230524922969835098721821&#13;&#10;Content-Disposition: form-data; name=&quot;data&quot;&#13;&#10;&#13;&#10;<?php phpinfo();&#13;&#10;----------------------------230524922969835098721821--&#13;&#10;',
                ErrorMessages::ITEM_NOT_FOUND,
                false,
                404
            ],
        ];
    }

    public function testUpdateBadFormData()
    {
        $this->loginUser();
        $filledItemData = sprintf('badData');
        $this->client->request('PUT', '/item', [], [], [], html_entity_decode($filledItemData));
        $this->assertResponseStatusCodeSame(400, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString(ErrorMessages::ERROR_MISSING_PARAMETER, $this->client->getResponse()->getContent());
    }

    public function testDelete()
    {
        $this->loginUser();

        $items = $this->itemRepository->findAll();
        /** @var Item $firstItem */
        $firstItem = $items[0];
        $this->assertCount(count($items), $this->itemRepository->findAll());
        $this->client->request('DELETE', sprintf('/item/%d', $firstItem->getId()));

        $this->assertResponseIsSuccessful();
        $this->assertCount(count($items) - 1, $this->itemRepository->findAll());
    }

    public function testDeleteWithNotFoundElement()
    {
        $this->loginUser();
        $items = $this->itemRepository->findAll();
        $this->assertCount(count($items), $this->itemRepository->findAll());
        $this->client->request('DELETE', sprintf('/item/%d', 999));

        $this->assertCount(count($items), $this->itemRepository->findAll());
        $this->assertResponseStatusCodeSame(404, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString(ErrorMessages::ITEM_NOT_FOUND, $this->client->getResponse()->getContent());
    }

    public function testDeleteNotLoggedIn()
    {
        $this->client->request('DELETE', sprintf('/item/%d', 999));
        $this->assertResponseStatusCodeSame(401, $this->client->getResponse()->getStatusCode());
    }

    private function loginUser(): void
    {
        /** @var UserInterface $user */
        $user = $this->userRepository->findOneByUsername('john');
        $this->client->loginUser($user);
    }
}
