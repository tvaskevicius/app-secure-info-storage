# Secure Information Storage REST API

### Project setup

* Add `secure-storage.localhost` to your `/etc/hosts`: `127.0.0.1 secure-storage.localhost`
* Setup .env to establish mysql connection
* Run `make init` to initialize project


* Open in browser: http://secure-storage.localhost:8000/item Should get `Full authentication is required to access this resource.` error, because first you need to make `login` call (see `postman_collection.json` or `SecurityController` for more info).
** Candidates note - port 8000 throws ECONNREFUSED. Intended? Port 80 works fine.
### Run tests

make tests

### API credentials

* User: john
* Password: maxsecure

### Postman requests collection

You can import all available API calls to Postman using `postman_collection.json` file

### Notes by candidate

API documentation is written in RAML, you can find it in the root folder api.raml. 
Also html representation of the API can be found in the api.html file.

Note on implementation: I have struggled to think of a decent way to parse form-data from the PUT request, I think it is probably bug-prone and I hope we can discuss this. Core class exists in src/Service/MultipartFormDataExtractorService.php

As for the decryption - I lacked a little time to benchmark how the encryption/decryption affects the speed, before no encryption requests with item manipulations were about 20ms (used Postman runner)

Things to consider moving forward:

* Some kind of cache pool (for now tried to eliminate queries to database as much as possible);
* Use libsodium for passwords, maybe data(A data migration would be needed, so this takes preparation);
* Invest more time into researching encryption - is it fast enough, maybe other solution is needed.

